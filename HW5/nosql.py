from pymongo import MongoClient

cluster = MongoClient("mongodb+srv://alex:qwerty123@cluster0.vxecj.mongodb.net/?retryWrites=true&w=majority")

db = cluster["sample_restaurants"]

collections = db["restaurants"]
'''
col = collections.find({"borough" : {"$in" : ["Manhattan","Staten Island","Brooklyn","Bronx", "Queens"]}})
#col = collections.find({})
cuisines = []
for i in col:
    cuisines.append(i["cuisine"])
    
print(set(cuisines)) # All cuisines in New York

cuisines = []
col = collections.find({"borough" : "Manhattan"})
for i in col:
    cuisines.append(i["cuisine"])
print(set(cuisines)) # All cuisines in Manhattan

col = collections.find({"cuisine" : {"$in" : ["Pizza","Pizza/Italian"]}, "borough" : "Manhattan"})
print(col)
for i in col:
    print(i) # Show all pizzerias in Manhattan
'''

result = collections.aggregate([
{ "$group": {
        "_id": {
            "borough": "$borough",
            "cuisine": "$cuisine"
        },
        "count": { "$sum": 1 }
    }}
])
for i in result:
    print(i) # Group the data by borough and cuisine, calculate counts and sort by count
