import pyspark
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import TimestampType, LongType
from datetime import datetime
spark = SparkSession.builder.getOrCreate()
df = spark.read.csv('hdfs://cluster-ff95-m/data/departuredelays.csv')
df = df.selectExpr("_c0 as date", "_c1 as delay")
df = df.withColumn('date', df['date'].cast(LongType()))
df = df.withColumn('date', month(df['date'].cast(TimestampType())))
df2 = df.groupBy('date').count()
df2.show()


