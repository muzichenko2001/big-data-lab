import pyspark
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from datetime import datetime
spark = SparkSession.builder.getOrCreate()
df = spark.read.csv('hdfs://cluster-ff95-m/data/departuredelays.csv', sep=r',')
df2 = spark.read.csv('hdfs://cluster-ff95-m/data/airport-codes-na.tsv', sep=r'\t')
df3 = spark.read.csv('hdfs://cluster-ff95-m/data/airport-codes-na.tsv', sep=r'\t')
df2 = df2.selectExpr("_c0 as dest_city", "_c1 as state", "_c2 as dest_country", "_c3 as IATA")
df3 = df3.selectExpr("_c0 as orig_city", "_c1 as state", "_c2 as orig_country", "_c3 as IATA")
df = df.selectExpr("_c0 as date", "_c1 as delay", "_c2 as distance", "_c3 as orig_code", "_c4 as dest_code")
df = df.join(df2, df.dest_code == df2.IATA)
df = df.join(df3, df.orig_code == df3.IATA)
df = df.sort('delay', ascending=False)
df.select("date","delay","orig_code","orig_city","orig_country","dest_code","dest_country","dest_city").show(10)


