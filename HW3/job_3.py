import pyspark
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from datetime import datetime
from pyspark.sql.types import LongType
spark = SparkSession.builder.getOrCreate()
df = spark.read.csv('hdfs://cluster-ff95-m/data/departuredelays.csv', sep=r',')
df2 = spark.read.csv('hdfs://cluster-ff95-m/data/airport-codes-na.tsv', sep=r'\t')

df2 = df2.selectExpr("_c0 as dest_city", "_c1 as state", "_c2 as dest_country", "_c3 as IATA")
df = df.selectExpr("_c0 as date", "_c1 as delay", "_c2 as distance", "_c3 as orig_code", "_c4 as dest_code")
df = df.withColumn('delay', df['delay'].cast(LongType()))
df = df.join(df2, df.dest_code == df2.IATA)
df = df.groupBy('dest_code',"dest_country","dest_city").avg('delay')
df = df.sort('avg(delay)', ascending=False)
df.select("dest_code","dest_country","dest_city","avg(delay)").show(10)
