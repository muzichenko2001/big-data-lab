from pyspark.shell import spark
from pyspark.sql.types import StructType, StructField, StringType, LongType, TimestampType
from pyspark.sql.functions import col,from_json, window, session_window

def count_word(x):
	return len(x.split(" "))


schema = StructType([ \
    StructField("speaker",StringType(),True),
    StructField("time",StringType(),True),
    StructField("words",StringType(),True)])
df = spark.readStream.format("kafka").option("kafka.bootstrap.servers", "localhost:9092").option("subscribe", "messages").option("startingOffsets", "earliest").load()
inf_df = df.selectExpr("CAST(value AS STRING)")


count_words = spark.udf.register("count_words", count_word)

inf_df = inf_df.withColumn("jsonData",from_json(col("value"),schema)).select("jsonData.*")

inf_df = inf_df.withColumn("words", count_words(col("words")))
inf_df = inf_df.withColumn("words", inf_df['words'].cast(LongType()))
inf_df = inf_df.withColumn("time", inf_df['time'].cast(TimestampType()))


inf = inf_df.withWatermark('time','10 days').groupby(window('time','10 days'),"speaker").sum("words")


inf.writeStream.format("console").outputMode("append").start().awaitTermination() 

