from pyspark.shell import spark
from pyspark.sql.types import StructType, StructField, StringType, LongType, TimestampType
from pyspark.sql.functions import col,from_json, window, session_window

file = open("bad_word.txt", "r")
bad_words = file.read().split("\n")
file.close()

def count_bad_word(x):
	mes = x.split(" ")
	result = []
	for i in mes:
		if i in bad_words:
			result.append(i)
	return len(result)


schema = StructType([StructField("speaker",StringType(),True),
    StructField("time",StringType(),True),
    StructField("words",StringType(),True)])
df = spark.readStream.format("kafka").option("kafka.bootstrap.servers", "localhost:9092").option("subscribe", "messages").option("startingOffsets", "earliest").load()
inf_df = df.selectExpr("CAST(value AS STRING)")


count_bad_word = spark.udf.register("count_bad_word", count_bad_word)

inf_df = inf_df.withColumn("jsonData",from_json(col("value"),schema)).select("jsonData.*")

inf_df = inf_df.withColumn("words",  count_bad_word(col("words")))
inf_df = inf_df.withColumn("words", inf_df['words'].cast(LongType()))
inf_df = inf_df.withColumn("time", inf_df['time'].cast(TimestampType()))

inf = inf_df.withWatermark('time','10 days').groupby(window('time','10 days'),"speaker").sum("words")


inf.writeStream.format("console").outputMode("append").start().awaitTermination() 
 
'''

