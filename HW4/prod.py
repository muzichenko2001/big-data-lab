from datetime import datetime
from datetime import timedelta
from random import choice, randrange
from kafka.producer import KafkaProducer
import json
import time


time_1 = datetime(2022, 6, 20, 23, 55, 59, 342380)


with open('words.txt', "r") as f:
    List_words = f.read().split("\n")


def generate_words(List_words):
    words = ""
    for i in range(0,randrange(0,20)):
        words+=choice(List_words)+" "
    return words


def serializer(mes):
    return json.dumps(mes).encode('utf-8')


def generate_messages(speaker_list, List_words):
    global time_1
    time_1 += timedelta(seconds=1)
    message = {
            "speaker": choice(speaker_list),
            "time":str(time_1),
            "words":generate_words(List_words)
        }
    return message


producer = KafkaProducer(
    bootstrap_servers =('localhost:9092'),
    value_serializer = serializer
    )

if __name__ == "__main__":
    while True:
        mes =  generate_messages(['Speaker1', 'Speaker2','Speaker3'], List_words)
        print('INFO:',datetime.now(),"|",mes)
        producer.send('messages', mes)
        time.sleep(2)

